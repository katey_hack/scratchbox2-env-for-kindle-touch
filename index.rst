.. Copyright 2012, Leonid Borisenko.

   This work is licensed under the Creative Commons Attribution-ShareAlike 3.0
   Unported License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to Creative
   Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041,
   USA.

Scratchbox2 environment for Kindle Touch development
====================================================

This is a "laboratory notes" on how to setup cross-compiling environment for Kindle Touch with using of scratchbox2.

Contents:

.. toctree::
   :maxdepth: 2

   content/crosscompilers.rst

License
-------

This work is licensed under a `Creative Commons Attribution-ShareAlike 3.0
Unported License <http://creativecommons.org/licenses/by-sa/3.0/>`_.
