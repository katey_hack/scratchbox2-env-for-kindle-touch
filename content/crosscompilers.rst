.. Copyright 2012, Leonid Borisenko.

   This work is licensed under the Creative Commons Attribution-ShareAlike 3.0
   Unported License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to Creative
   Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041,
   USA.

Install cross-compilers
=======================

Cross-compiling is a process for producing binary code for *target* system,
while compiling source code on *host* system. So, for example, one could
compile source code on powerful computer with Intel x86 processor, while
producing code for relatively "weak" ARM-based computer (like Kindle Touch).

GCC
---

GCC_ includes compilers for C and C++ (and some others programming languages).

Not only compiler is needed for producing of finished binary code, but also
some other utilites (like assembler, linker). GNU compiler from GCC combined
with these utilities is called *GNU toolchain*.

There are two ways to install cross-compiling GNU toolchain: either compile it
from sources (maybe, by automated process with the help of specialized tools,
like crosstool-NG_), or install a pre-built toolchain.

Popular pre-built GNU toolchains, suitable for Kindle Touch development, are:

* Linaro_
* `Sourcery CodeBench Lite Edition`_ (built for *GNU/Linux* target OS)

.. note::
   Stock Kindle Touch binary files were built with following GNU toolchains
   (number shows how many files were built with this toolchain)::

      799    (Linaro GCC 4.5-2011.05-0) 4.5.4 20110505 (prerelease)
       11    (4.4.4_09.06.2010) 4.4.4
        1    (Ubuntu 4.4.3-4ubuntu5) 4.4.3

   This information was extracted with the following shell command:

   .. code-block:: sh

      find /tmp/kindle-touch-5.1.2-rootfs -type f \
      | xargs arm-linux-gnueabi-readelf --string-dump=.comment 2>/dev/null \
      | awk '/0\]\s+GCC:/ { for (i = 1; i < 4; i++) $i = ""; print }' \
      | sort | uniq --count | sort --numeric-sort --reverse

   `(4.4.4_09.06.2010) 4.4.4` is a Freescale-provided toolchain. Freescale is
   the producer of ARM-based system on chip installed in Kindle Touch.

There is some value in using the same toolchain that had been used for stock
system (less incompatibilities in compiling process), but newer toolchins also
bring some gains (more capabilities).

Install cross GNU toolchain from APT repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Information above about available pre-built GNU toolchains was provided just
for reference. Fortunately, there is APT repository with recent GCC
cross-compilers, so they could be installed in Debian via standard `apt-get`.
This repository is supported by `emdebian.net`_.

Install emdebian keyring package:

.. code-block:: console

   # apt-get install emdebian-archive-keyring

Create file :file:`/etc/apt/sources.list.d/emdebian.list`:

.. code-block:: sources.list

   # emdebian.net cross toolchains
   deb http://www.emdebian.org/debian/ unstable main
   deb-src http://www.emdebian.org/debian/ unstable main

Update packages cache:

.. code-block:: console

   # apt-get update

Install C and C++ cross-compilers:

.. code-block:: console

   # apt-get install gcc-4.7-arm-linux-gnueabi g++-4.7-arm-linux-gnueabi

Now it's possible to cross-compile primitive C programs, like
:file:`helloworld.c`:

.. code-block:: c

   #include <stdio.h>

   int main() {
       printf("Hello world!");
       return 0;
   }

with command:

.. code-block:: console

   $ arm-linux-gnueabi-gcc-4.7 helloworld.c -o helloworld

Go
--

Google's original Go compiler (:program:`gc`) is inherently a cross-compiler.
It's very easy to setup cross-compiling ARM-targeted :program:`gc`.

However, it's not so easy to enable interoperability with C code in
cross-compiling :program:`gc`. This feature is called cgo_ and there is no
official way to enable `cgo` when cross-compiling with :program:`gc`.

So it's necessary to perform some additional manipulations according to
`this post`_ from `golang-nuts` group.

For applying of required manipulations, :program:`gc` must be built from
sources. For the reference, here are official instructions about this process:
http://golang.org/doc/install/source. But step-by-step instructions also
listed below.

Clone repository with Go source
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Install `Mercurial SCM`_ and CA certificates, required for getting source code
of :program:`gc`, and compiler infrastructure, required for building of
:program:`gc`:

.. code-block:: console

   # apt-get install mercurial ca-certificates gcc libc6-dev

Enable `progress`_ Mercurial extension, so that cloning of repository will
proceed with displaying of informative progress bar. Add following lines to
file :file:`.hgrc` in your home directory:

.. code-block:: ini

   [extensions]
   progress =

Clone repository with :program:`gc` source code and update to `tip` revision:

.. code-block:: console

   $ hg clone --updaterev tip https://code.google.com/p/go

`Tip` revision is a last commited revision, so it's a bleeding-edge source.
`Tip` is required, because :program:`gc` from `release` revision (currently
pointed at `go1.0.3`) doesn't support some ARM ELF relocations_. This lack of
support will result in errors in compilation with `cgo`.

Bootstrap Go tree
^^^^^^^^^^^^^^^^^

Go tree is a set of compiler, tools, standard library etc. Building
process should be customized with environment variables:

* :envvar:`GOROOT_FINAL` -- path to directory, where built Go tree will be
  located. It's optional; default value is the directory where source
  repository had been cloned

* :envvar:`GOARCH` -- target architecture for compiled Go programs

* :envvar:`GOOS` -- target OS for compiled Go programs

* :envvar:`GOARM` -- this environment variable controls emitting of VFP
  (hardware floating point unit) instructions in compiling to ARM binaries.
  `5` means using only software floating point implementation, `6` -- emitting
  VFP instructions, suitable for ARMv6 architecture, and `7` -- emitting
  also VFP instructions available on ARMv7 architecture.

(Full list of environment variables controlling building process could be
found in official instructions about building Go compiler from sources.)

.. code-block:: console

   $ cd go/src
   $ GOROOT_FINAL=$HOME/go-for-kt GOARCH=arm GOOS=linux GOARM=7 ./make.bash
   $ cd ..

Copy built Go tree to final destination (required only if
:envvar:`GOROOT_FINAL` was set at building):

.. code-block:: console

   $ mkdir -p $HOME/go-for-kt
   $ for what in api bin doc favicon.ico lib pkg robots.txt src; do \
   >   cp -r $what $HOME/go-for-kt \
   > done
   $ cd $HOME/go-for-kt

Make fake "native" compile of Go with `cgo` enabled
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now (quoting from post, referenced earlier) trick command:`cmd/go` into
thinking it is doing native compile so that it will allow to enable `cgo`.

Make GNU C cross-compiler available under the name :command:`gcc`. Create file
:file:`bin/gcc` with the following content:

.. code-block:: sh

   #!/bin/sh
   /usr/bin/arm-linux-gnueabi-gcc-4.7 "$@"

Make it executable:

.. code-block:: console

   $ chmod u+x bin/gcc

Rebuild :program:`gc` with `cgo` enabled and :command:`gcc` cross-compiler
in :envvar:`$PATH`:

.. code-block:: console

   $ PATH="$PWD/bin:$PATH" GOARCH=arm GOOS=linux GOARM=7 CGO_ENABLED=1 bin/go install -a -v std

Remove build artifacts (to considerably decrease built Go tree size without
loss of any cross-compiling functionality):

* :file:`bin/linux_arm`, :file:`pkg/tool/linux_arm` -- command tools built for
  executing on ARM architecture

* :file:`pkg/obj`, :file:`pkg/$GOHOSTOS_$GOHOSTARCH` -- libraries for native
  (non-cross-compile) build

.. code-block:: console

  $ rm -rfv bin/linux_arm pkg/tool/linux_arm pkg/obj "pkg/$(bin/go env GOHOSTOS)_$(bin/go env GOHOSTARCH)"

Wrap :command:`bin/go`
^^^^^^^^^^^^^^^^^^^^^^

Now, wrap real :command:`bin/go` command into shell script with:

* prepending to :envvar:`$PATH` path to :file:`bin` directory with
  :command:`gcc` cross-compiler
* setting appropriate environment variables to enable cross-compiling
* aborting when :envvar:`$GOPATH` is undefined. By default, :envvar:`$GOPATH`
  is set to value of :envvar:`$GOROOT`, i.e. to directory containing built Go
  tree. It means that all user-installed and user-built packages and
  executables will be placed into :envvar:`$GOROOT`. But it's better to place
  them into some other directory to make distinction between standard and
  user-installed tools/packages. Read also `official $GOPATH documentation`_.

.. note::

   :envvar:`$GOARM` variable set at Go tree build time is embedded into Go
   linker and applied by default. It is `recent addition to Go`_.
   :envvar:`$GOARM` still could be set at cross-compiling to override default
   value.

   :envvar:`$CGO_ENABLED` value set at Go tree build time is also applied by
   default and also could be overrided.

Rename :file:`bin/go`:

.. code-block:: console

   $ mv bin/go bin/true.go.binary

Create new file :file:`bin/go` with the following content (change
:envvar:`$BINDIR` definition appropriately):

.. code-block:: sh

   #!/bin/sh
   if [ "x$1" = "xget" -o "x$1" = "xinstall" ]; then
     : ${GOPATH:?"undefined; default to GOROOT, but it's bad"}
   fi
   BINDIR=$HOME/go-for-kt/bin
   PATH="${BINDIR}:${PATH}" GOARCH=arm GOOS=linux ${BINDIR}/true.go.binary "$@"

Make it executable:

.. code-block:: console

   $ chmod u+x bin/go

Now it's possible to cross-compile Go programs with (or without) `cgo`. by
invoking :file:`$HOME/go-for-kt/bin/go`.

There is one example of Go program with `cgo` in original Go source. It's
located at :file:`misc/cgo/life/main.go`. If source was cloned into 
:file:`/tmp/go`, execute:

.. code-block:: console

   $ $HOME/go-for-kt/bin/go build /tmp/go/misc/cgo/life/main.go

It must proceed without errors and produce ARM-targeted :program:`main`
executable in current directory:

.. code-block:: console

   $ arm-linux-gnueabi-readelf --file-header main | grep Machine
     Machine:                           ARM

.. note::
   Starting from GCC 4.7 there is also Go compiler included. It's available in
   `emdebian.net`_ repository and could be installed with command:

   .. code-block:: console

      # apt-get install gccgo-4.7-arm-linux-gnueabi

   and then Go programs could be cross-compiled with:

   .. code-block:: console

      $ arm-linunx-gnueabi-gccgo-4.7 program.go -o program

   (Look also at `official instructions about using of gccgo`_.)

   `gccgo` from GCC 4.7.x couldn't be used with :command:`go` tool from `tip`
   revision of :program:`gc` source (like `go -compiler gccgo`), because
   `tip` :command:`go` expects some options introduced only in GCC 4.8
   (which is currently not available in `emdebian.net` repository).

.. _GCC: http://gcc.gnu.org
.. _crosstool-NG: http://crosstool-ng.org
.. _Linaro: https://launchpad.net/linaro-toolchain-binaries
.. _Sourcery CodeBench Lite Edition: https://sourcery.mentor.com/sgpp/lite/arm/portal/subscription?@template=lite
.. _emdebian.net: http://www.emdebian.org/crosstools.html
.. _cgo: http://golang.org/doc/articles/c_go_cgo.html
.. _this post: https://groups.google.com/d/msg/golang-nuts/ESQ0_yxH130/89_eAFyCurUJ
.. _Mercurial SCM: http://mercurial.selenic.com/
.. _progress: http://mercurial.selenic.com/wiki/ProgressExtension
.. _relocations: http://en.wikipedia.org/wiki/Relocation_(computing)
.. _official $GOPATH documentation: http://golang.org/cmd/go/#GOPATH_environment_variable
.. _recent addition to Go: http://code.google.com/p/go/source/detail?r=65241fa50264
.. _official instructions about using of gccgo: http://golang.org/doc/install/gccgo#Using_gccgo
